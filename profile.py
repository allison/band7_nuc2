#!/usr/bin/python

"""
This profile allows the allocation of resources for over-the-air
operation on the POWDER platform. Specifically, the profile has
options to request the allocation of SDR radios in fixed-endpoints
(i.e., nodes deployed at human height).

Map of deployment is here:
https://www.powderwireless.net/map

The fixed-endpoint SDRs are B210s each of which is paired with 
an Intel NUC small form factor compute node. Both B210s are connected
to broadband antennas: nuc1 is connected in an RX only configuration,
while nuc2 is connected in a TX/RX configuration.

The profile uses a disk image with GNU Radio and the UHD software tools, pre-installed.

Resources needed to realize a basic SDR monitoring setup:

 * A "nuc2" fixed-end point compute/SDR pair. (This will run the uhd monitoring software.)
  
**Specific resources that can be used with this profile:** 

 * Hardware (at least one set of resources are needed):
  * Humanities, nuc2
  * Law 73, nuc2
  * Moran, nuc2
  * Sage Point, nuc2

Instructions:

#### To run uhd monitoring software

  * Select a center frequency "close" to the range you want to observe. E.g., suppose you want to monitor the frequencies associated with the downstream range of LTE band-7, which goes from 2620 MHz - 2690 MHz. In this case you might select a center frequency of 2655 MHz.

  * Select a sampling rate appropriate for the SDR in your experiment. (For fixed-endpoints use a sample rate of 50 MS/s, i.e., `-s 50M`. For base stations use a sample rate of 100 MS/s, i.e., `-s 100M`.)
  
  * Run "uhd_fft -s SAMPLE_RATE -f CENTER_FREQUENCY" to monitor the spectrum around the center frequency. E.g., for the LTE band-7 example, with an "fixed-endpoint" SDR:
  
  `sudo uhd_fft -s 50M -f 2655M`

  Note: In order to see the GUI of the uhd_fft software you need to access you experiment using ssh *and* have X11 configured on laptop/desktop. 

"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum

x310_node_disk_image = \
		"urn:publicid:IDN+emulab.net+image+PowderProfiles:U18-GR-SRS-x310"
b210_node_disk_image = \
		"urn:publicid:IDN+emulab.net+image+PowderProfiles:U18-GR-SRS-b210"


def x310_node_pair(idx, x310_radio, node_type, installs):
    radio_link = request.Link("radio-link-%d"%(idx))
    radio_link.bandwidth = 10*1000*1000

    node = request.RawPC("%s-comp"%(x310_radio.radio_name))
    node.hardware_type = node_type
    node.disk_image = x310_node_disk_image
    node.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"

    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)

    radio = request.RawPC("%s-x310"%(x310_radio.radio_name))
    radio.component_id = x310_radio.radio_name
    radio_link.addNode(radio)


def b210_nuc_pair(idx, b210_node, installs):
    b210_nuc_pair_node = request.RawPC("b210-%s-%s"%(b210_node.aggregate_id,"nuc2"))
    agg_full_name = "urn:publicid:IDN+%s.powderwireless.net+authority+cm"%(b210_node.aggregate_id)
    b210_nuc_pair_node.component_manager_id = agg_full_name
    b210_nuc_pair_node.component_id = "nuc2"
    b210_nuc_pair_node.disk_image = b210_node_disk_image


fixed_endpoint_aggregates = [
    ("web",
     "WEB, nuc2"),
    ("ebc",
     "EBC, nuc2"),
    ("bookstore",
     "Bookstore, nuc2"),
    ("humanities",
     "Humanities, nuc2"),
    ("law73",
     "Law 73, nuc2"),
    ("madsen",
     "Madsen, nuc2"),
    ("sagepoint",
     "Sage Point, nuc2"),
    ("moran",
     "Moran, nuc2"),
    ("cpg",
     "Garage, nuc2"),
    ("guesthouse",
     "Guesthouse, nuc2")
]

portal.context.defineStructParameter("b210_nodes", "B210 Radios", [],
                                     multiValue=True,
                                     min=0, max=None,
                                     members=[
                                         portal.Parameter(
                                             "aggregate_id",
                                             "Fixed Endpoint B210",
                                             portal.ParameterType.STRING,
                                             fixed_endpoint_aggregates[4],
                                             fixed_endpoint_aggregates)
                                     ],
                                    )

params = portal.context.bindParameters()

request = portal.context.makeRequestRSpec()

installs = []

for i, b210_node in enumerate(params.b210_nodes):
    b210_nuc_pair(i, b210_node, installs)

portal.context.printRequestRSpec()
